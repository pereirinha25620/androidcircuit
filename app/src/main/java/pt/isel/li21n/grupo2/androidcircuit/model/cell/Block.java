package pt.isel.li21n.grupo2.androidcircuit.model.cell;

import pt.isel.li21n.grupo2.androidcircuit.model.Dir;
import pt.isel.li21n.grupo2.androidcircuit.model.Position;

public class Block extends Cell {

    public Block(char type, Position pos) {
        super(type, pos);
    }

    @Override
    public boolean isConnected() {
        return true;
    }

    @Override
    public boolean connectCell(Dir dir) {
        return false;
    }

    @Override
    public boolean unlinkCell(Dir dir) {
        return false;
    }

    @Override
    public Dir getNextDirection(Dir dir) {
        return null;
    }

    @Override
    public void resetCell() {
    }

    @Override
    public boolean drag(Dir dir) {
        return false;
    }
}
