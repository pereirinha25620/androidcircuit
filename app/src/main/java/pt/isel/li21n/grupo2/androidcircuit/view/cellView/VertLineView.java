package pt.isel.li21n.grupo2.androidcircuit.view.cellView;

import android.graphics.Canvas;

import pt.isel.li21n.grupo2.androidcircuit.model.Dir;
import pt.isel.li21n.grupo2.androidcircuit.model.cell.Cell;

public class VertLineView extends CellView {

    private static final Dir[] VERTICAL_DIR = {Dir.LEFT, Dir.RIGHT};
    protected static final float[] VERT_DIR_TO_COORDS = {
            2*CENTER_RADIUS,    // TOP
            -2*CENTER_RADIUS,   // DOWN
    };


    public VertLineView(Cell cell) {
        super(cell);
    }

    @Override
    protected void drawDefaultCell(Canvas canvas, int side) {
        defaultPaint.setStrokeWidth(side * 2 * CENTER_RADIUS);

        for (Dir dir : VERTICAL_DIR)
            canvas.drawLine(
                    side/2, side/2,                                             // FROM
                    side/2, side/2 + side*VERT_DIR_TO_COORDS[dir.ordinal()],    // TO
                    defaultPaint
            );
    }

    @Override
    protected void drawConnections(Canvas canvas, int side) {
        for (Dir dir : cell.getConnections())
            canvas.drawLine(
                    side/2, side/2,
                    side*CellView.DIR_TO_COORDS[dir.ordinal()*2], side*CellView.DIR_TO_COORDS[dir.ordinal()*2+1],
                    cellPaint
            );
    }
}
