package pt.isel.li21n.grupo2.androidcircuit.view.cellView;

import android.graphics.Canvas;

import pt.isel.li21n.grupo2.androidcircuit.model.Dir;
import pt.isel.li21n.grupo2.androidcircuit.model.cell.Cell;

public class FreeView extends CellView {

    public FreeView(Cell cell) {
        super(cell);
    }

    @Override
    protected void drawDefaultCell(Canvas canvas, int side) {
        canvas.drawCircle(side/2, side/2, side * CENTER_RADIUS, defaultPaint);
    }

    protected void drawConnections(Canvas canvas, int side) {
        for (Dir dir : cell.getConnections())
            canvas.drawLine(
                    side/2, side/2,
                    side*CellView.DIR_TO_COORDS[dir.ordinal()*2], side*CellView.DIR_TO_COORDS[dir.ordinal()*2+1],
                    cellPaint
            );

        canvas.drawCircle(side/2, side/2, side * CENTER_RADIUS, cellPaint);
    }
}
