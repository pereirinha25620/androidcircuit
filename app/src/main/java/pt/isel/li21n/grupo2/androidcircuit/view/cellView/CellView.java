package pt.isel.li21n.grupo2.androidcircuit.view.cellView;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import pt.isel.li21n.grupo2.androidcircuit.model.cell.Cell;
import pt.isel.poo.tile.Tile;

public abstract class CellView implements Tile {

    protected static final float CENTER_RADIUS = 1f/7;
    protected static final float TERMINAL_RADIUS = 2f/5;
    protected static final int CENTER_COLOR = Color.argb(0xff, 0x21, 0x21, 0x21);

    // Matches between the color number in the model and the color displayed on the Color
    private static final int[] COLORS = {
            Color.RED, Color.GREEN, Color.YELLOW,
            Color.BLUE, Color.MAGENTA, Color.CYAN
    };


    // Vectors used to draw cell connections according to directions
    protected static final float[] DIR_TO_COORDS = {
                0,      0.5f,   // LEFT
                1,      0.5f,   // RIGHT
                0.5f,   1,      // DOWN
                0.5f,   0       // UP
    };


    Cell cell;
    Paint cellPaint, defaultPaint, highlightPaint;
    boolean selected;

    public CellView(Cell cell) {
        this.cell = cell;
        cellPaint = new Paint();
        defaultPaint = new Paint();
        defaultPaint.setColor(CENTER_COLOR);
        highlightPaint = new Paint();
        highlightPaint.setColor(Color.GREEN);
        highlightPaint.setStyle(Paint.Style.STROKE);
        highlightPaint.setStrokeWidth(5);
        selected = false;
    }


    public static CellView newInstance(Cell cell) {
      try {
            Class vc = Class.forName(
                    "pt.isel.li21n.grupo2.androidcircuit.view.cellView."+cell.getClass().getSimpleName()+"View"
            );
            return (CellView) vc.getConstructor(Cell.class).newInstance(cell);
        } catch (Exception e) { return null; }
    }



    /* ---------------- DRAW DEFAULT CELLVIEW ---------------- */
    @Override
    public void draw(Canvas canvas, int side) {
        if (cell.getConnections().size() < 2)
            drawDefaultCell(canvas, side);

        if (cell.getConnections().size() > 0) {
            setColor(CellView.getColor(cell));
            cellPaint.setStrokeWidth(side*2*CENTER_RADIUS);

            drawConnections(canvas, side);
        }

        if (selected)
            highlightCell(canvas, side);
    }

    protected abstract void drawDefaultCell(Canvas canvas, int side);

    protected abstract void drawConnections(Canvas canvas, int side);

    public void highlightCell(Canvas canvas, int side) {
        canvas.drawRect(0, 0, side, side, highlightPaint);
    }


    public Cell getCell() {
        return cell;
    }

    public void setCell(Cell cell) {
        this.cell = cell;
    }


    public static int getColor(Cell cell) {
        if (cell.getColor() < 0 || cell.getColor() >= COLORS.length) {
            return Color.GRAY;
        }
        return COLORS[ cell.getColor() ];
    }

    public void setColor(int color) {
        cellPaint.setColor(color);
    }


    @Override
    public boolean setSelect(boolean selected) {
        return this.selected = selected;
    }
}
