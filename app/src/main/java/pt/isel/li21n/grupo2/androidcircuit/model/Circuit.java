package pt.isel.li21n.grupo2.androidcircuit.model;

import java.util.LinkedList;

import pt.isel.li21n.grupo2.androidcircuit.model.cell.*;

public class Circuit {

    private Cell[][] circuit;

    public Circuit(int height, int width) {
        circuit = new Cell[height][width];
    }

    public int getLines() {
        return circuit.length;
    }

    public int getColumns() {
        return circuit[0].length;
    }

    public void putCell(int lin, int col, Cell cell) {
        circuit[lin][col] = cell;
    }

    public Cell getCell(int lin, int col) {
        return circuit[lin][col];
    }

    public Cell getCell(Position pos) {
        return circuit[(int) pos.getLine()][(int) pos.getCol()];
    }



    /* ***************************************************************
     *                     CONNECT RULES
     * ***************************************************************/

    // Method to be called from controller on drag event
    public boolean drag(Position from, Position to) {
        Dir moveDir = getDirection(from, to);
        Cell fromCell = getCell(from);
        Cell toCell = getCell(to);

        // Check if both cells are compatible with move direction
        if (cellDirectionsAreCompatible(fromCell, toCell, moveDir)) {
            // Check for cell connection possibility
            if (canDragAndConnect(fromCell, toCell, moveDir))
                return true;
                // Check for cell possible unlink
            else if (canDragAndUnlink(fromCell, toCell, moveDir))
                return true;
                // Check for path merging possibility
            else if (mergeCellInPath(fromCell, toCell, moveDir))
                return true;
        }
        return false;
    }

    // Validate if two cells can connect on a given direction
    private boolean cellDirectionsAreCompatible(Cell fromCell, Cell toCell, Dir moveDir) {
        return fromCell.drag(moveDir) && toCell.drag(moveDir);
    }

    // Validate and update model state on Drag event if Cells can connect on a given direction
    private boolean canDragAndConnect(Cell fromCell, Cell toCell, Dir moveDir) {
        boolean fromUnconnectedTerminal = fromCell instanceof Terminal && !fromCell.isConnected();
        boolean fromUnconnectedNodeInPath = fromCell instanceof Node && fromCell.isInPath() && !fromCell.isConnected();
        boolean toFreeCell = !toCell.isConnected() && !toCell.isInPath();

        /*
         MOVE FROM: an Unconnected Terminal or Unconnected CellView In Path
                TO: Free Cell
         */
        if ( (fromUnconnectedTerminal || fromUnconnectedNodeInPath) && toFreeCell) {
            // Destination Cell is: NODE
            if (toCell instanceof Node) {
                ((Node) toCell).updateType(fromCell.getType());
            }
            // Destination Cell is: Terminal
            else {
                // And is same type as path
                if (fromCell.getType() != toCell.getType())
                    return false;
            }
            // Update FROM and TO cells
            fromCell.connectCell(moveDir);
            toCell.connectCell(moveDir.invertDir());
            return true;
        }
        return false;
    }

    // Validate and update model state on Drag event if Cells can unlink on a given direction
    private boolean canDragAndUnlink(Cell fromCell, Cell toCell, Dir moveDir) {
        /*
         MOVE FROM: Cell is not in a path yet
                OR: Cells are of different type
                Not eligible for unlinking.
         */
        if ( !fromCell.isInPath() || fromCell.getType() != toCell.getType())
            return false;

        boolean fromUnconnectedNodeInPath = fromCell instanceof Node && fromCell.isInPath() && !fromCell.isConnected();
        boolean toConnectedCell = toCell.isConnected();

        Dir toDir = toCell.getConnection();
        boolean directionsAreCompatible = moveDir == toDir.invertDir();

        /*
         MOVE FROM: Unconnected (one free connection) CellView In Path
                TO: Connected Cell in same Path
         */
        if ( fromUnconnectedNodeInPath && toConnectedCell && directionsAreCompatible) {
            fromCell.unlinkCell(moveDir);
            toCell.unlinkCell(toDir);
            return true;
        }
        return false;
    }

    // Validate and update model state on Drag event if paths can merge on a given direction
    private boolean mergeCellInPath(Cell fromCell, Cell toCell, Dir moveDir) {
        boolean fromFreeTerminal = fromCell instanceof Terminal && !fromCell.isConnected();
        boolean fromUnconnectedNodeInPath = fromCell instanceof Node && fromCell.isInPath() && !fromCell.isConnected();
        boolean toUnconnectedNodeInPath = toCell instanceof Node && toCell.isInPath() && !toCell.isConnected();

        /*
         MOVE FROM: Free Terminal or unconnected CellView In Path
                TO: Unconnected Cell in same Path
         */
        if ((fromFreeTerminal || fromUnconnectedNodeInPath) && toUnconnectedNodeInPath) {
            if (fromCell.getType() == toCell.getType()) {
                fromCell.connectCell(moveDir);
                toCell.connectCell(moveDir.invertDir());
                return true;
            }
        }

        return false;
    }



    /* ***************************************************************
     *                     UNLINK RULES
     * ***************************************************************/

    // Apply unlink action to a given position, resulting in a single cell or path unlinking
    public boolean unlink(Position pos) {
        Cell curCell = getCell(pos);

        // CELL IS NOT CONNECTED
        if (!curCell.isInPath()) {
            return false;
        }

        // Get a path from the current cell
        Dir dir = curCell.getConnection();
        LinkedList<Cell> path = getPath(pos, dir);

        // SELECTED CELL IS: NODE
        if (curCell instanceof Node) {
            // TWO PATHS AVAILABLE FROM NODE
            if (curCell.isConnected()) {
                // Get alternative path
                Dir inverseDir = curCell.getNextDirection(dir.invertDir());
                LinkedList<Cell> inversePath = getPath(pos, inverseDir);
                // Evaluate second path: if second path ends on an open CellView clear the path and exit. Else, continue evaluating initial path.
                if (inversePath.getLast() instanceof Node) {
                    unlinkPath(pos, inverseDir, inversePath);
                    Position nextPos = new Position(curCell.getLine() + dir.getDeltaLin(), curCell.getCol() + dir.getDeltaCol());
                    unlinkCell(nextPos, dir.invertDir());
                    return true;
                }
            }
            // ONE PATH AVAILABLE
            if (path.getLast() instanceof Terminal) {
                // Current CellView is last in path: unlink current Cell
                unlinkCell(pos, dir);
            } else {
                // Current CellView is middle of path: unlink path from curCell to last free node
                unlinkPath(pos, dir, path);
            }
        }
        // SELECTED CELL IS: TERMINAL
        else {
            if (path.getLast() instanceof Terminal) {
                // Unlink closed path on current Terminal
                unlinkCell(pos, dir);
            } else {
                // Clear full path from first terminal to last free node
                unlinkPath(pos, dir, path);
            }
        }
        return true;
    }

    // Unlink a single Cell on a given position
    private void unlinkCell(Position pos, Dir dir) {
        Cell curCell = getCell(pos);
        Position nextPos = new Position(curCell.getLine() + dir.getDeltaLin(), curCell.getCol() + dir.getDeltaCol());
        Cell nextCell = getCell(nextPos);
        curCell.unlinkCell(dir);
        nextCell.unlinkCell(dir.invertDir());
    }

    // Unlink a path starting on a given position
    private void unlinkPath(Position pos, Dir dir, LinkedList<Cell> path) {
        getCell(pos).unlinkCell(dir);
        path.removeFirst();
        for (Cell cell : path) {
            cell.resetCell();
        }
    }


    // Return a path of cells connected to each other from a given position
    private LinkedList<Cell> getPath(Position pos, Dir dir) {
        LinkedList<Cell> path = new LinkedList<>();
        Cell curCell = getCell(pos);
        while (true) {
            path.add(curCell);
            Position nextPos = new Position(curCell.getLine() + dir.getDeltaLin(), curCell.getCol() + dir.getDeltaCol());
            Cell nextCell = getCell(nextPos);
            dir = nextCell.getNextDirection(dir);

            if (nextCell.isConnected() && nextCell instanceof Node) {
                curCell = nextCell;
            } else {
                path.add(nextCell);
                break;
            }
        }
        return path;
    }

    // Return move direction based on relative cell positions
    private Dir getDirection(Position from, Position to) {
        if ((from.getLine() - to.getLine()) == 0) {
            if ((from.getCol() - to.getCol()) > 0) {
                return Dir.LEFT;
            } else {
                return Dir.RIGHT;
            }
        }
        else {
            if ((from.getLine() - to.getLine()) > 0) {
                return Dir.UP;
            } else {
                return Dir.DOWN;
            }
        }
    }



    // Validates if all cells are connected and if level is over
    public boolean isOver() {
        for (int l = 0; l < circuit.length; l++) {
            for (int c = 0; c < circuit[l].length; c++) {
                if (!getCell(l, c).isConnected())
                    return false;
            }
        }
        return true;
    }
}

