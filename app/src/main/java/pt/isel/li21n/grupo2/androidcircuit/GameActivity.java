package pt.isel.li21n.grupo2.androidcircuit;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

import pt.isel.li21n.grupo2.androidcircuit.model.Circuit;
import pt.isel.li21n.grupo2.androidcircuit.model.Dir;
import pt.isel.li21n.grupo2.androidcircuit.model.LevelFileAccess;
import pt.isel.li21n.grupo2.androidcircuit.model.Loader;
import pt.isel.li21n.grupo2.androidcircuit.model.Position;
import pt.isel.li21n.grupo2.androidcircuit.model.cell.Cell;
import pt.isel.li21n.grupo2.androidcircuit.view.Panel;
import pt.isel.li21n.grupo2.androidcircuit.view.cellView.CellView;
import pt.isel.poo.tile.OnBeatListener;
import pt.isel.poo.tile.OnTileTouchListener;

public class GameActivity extends Activity {

    private static final String STATE_LEVEL = "currentLevel";
    private static final String STATE_CELLS = "cellAt";
    private static final String STATE_TIMER = "timerValue";
    private static final String STATE_ELAPSEDTIME = "elapsedTimeValue";
    private static final String STATE_LEVEL_COMPLETED = "levelCompletedValue";
    private static final String STATE_NEXT_LEVEL_BUTTON = "nextLvlBtnEnabled";
    private static final String LAST_LEVEL_FILENAME = "lastLevel.txt";

    TextView timerTxtView;
    Button nextLevelBtn;
    TextView levelTxtView;
    Panel panel;
    Circuit model;
    int level;
    boolean levelCompleted = false;
    long elapsedTime = 0;

    /* --------------------------- LIFECYCLE MANAGEMENT SECTION ---------------------------   */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_game);

        /* ---- LOAD LAST COMPLETED LEVEL ---- */
        loadLastLevel();

        /* ---- LOAD CIRCUIT MODEL ---- */
        try {
            loadLevel(++level);
        } catch (Loader.LevelFormatException e) {
            Log.e("Loader", e.getMessage());
        }

        /* ---- LOAD VIEWS ---- */
        loadViewsById();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!levelCompleted)
            panel.setHeartbeatListener(1000, startBeatListener());
    }

    @Override
    protected void onPause() {
        super.onPause();

        panel.removeHeartbeatListener();
    }

    /* --------------------------- SAVE / RESTORE STATE ------------------------   */
    String[] curCellFields;
    char curCellType;

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        /* ---- LOAD TIMER VALUE ---- */
        elapsedTime = savedInstanceState.getLong(STATE_ELAPSEDTIME);
        timerTxtView.setText(savedInstanceState.getCharSequence(STATE_TIMER));

        /* ---- LOAD CURRENT LEVEL ---- */
        nextLevelBtn.setEnabled(
                savedInstanceState.getBoolean(STATE_NEXT_LEVEL_BUTTON)
        );
        levelCompleted = savedInstanceState.getBoolean(STATE_LEVEL_COMPLETED);
        level = savedInstanceState.getInt(STATE_LEVEL);

        /* ---- LOAD CURRENT CELL STATE ---- */
        try {
            loadLevel(level);
        } catch (Loader.LevelFormatException e) {
            Log.e("Loader", e.getMessage());
        }

        for (int l = 0; l < model.getLines(); l++)
            for (int c = 0; c < model.getColumns(); c++) {
                curCellFields = savedInstanceState.getString(STATE_CELLS + String.valueOf(l) + String.valueOf(c)).split(";");

                curCellType = curCellFields[curCellFields.length-1].length() == 1 ?
                        curCellFields[curCellFields.length-1].charAt(0) : curCellFields[0].charAt(0);

                model.putCell(
                        l,c,
                        Cell.newInstance(
                                curCellType,
                                new Position(curCellFields[1])
                        )
                );

                model.getCell(l,c).setType(curCellFields[0].charAt(0));

                if (curCellFields.length > 2) {
                    for (int d = 2; d < curCellFields.length; d++) {
                        if (curCellFields[d].length() <= 1)
                            break;
                        model.getCell(l,c).addConnection(Dir.valueOf(curCellFields[d]));
                    }
                }
            }

        for (int l = 0; l < model.getLines(); l++)
            for (int c = 0; c < model.getColumns(); c++)
                panel.setTile(c, l, CellView.newInstance(model.getCell(l, c)));

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        /* ---- SAVE TIMER VALUE ---- */
        outState.putLong(STATE_ELAPSEDTIME, elapsedTime);
        outState.putCharSequence(STATE_TIMER, timerTxtView.getText());

        /* ---- SAVE CURRENT LEVEL STATE ---- */
        outState.putBoolean(STATE_LEVEL_COMPLETED, levelCompleted);
        outState.putBoolean(STATE_NEXT_LEVEL_BUTTON, nextLevelBtn.isEnabled());
        outState.putInt(STATE_LEVEL, level);

        /* ---- SAVE CURRENT CELL STATE ---- */
        for (int l = 0; l < model.getLines(); l++)
            for (int c = 0; c < model.getColumns(); c++)
                outState.putString(STATE_CELLS + String.valueOf(l) + String.valueOf(c), model.getCell(l, c).toString());
    }


    /* --------------------- Back Button Quit Confirmation -------------------   */

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.QuitGameTitle)
                .setMessage(R.string.QuitGameMsg)
                .setPositiveButton(R.string.QuitGameYes, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton(R.string.QuitGameNo, null)
                .show();

    }

    /* --------------------------- PANEL LISTENER ---------------------------   */

    Position from = new Position();
    Position to = new Position();

    OnTileTouchListener panelListener = new OnTileTouchListener() {

        @Override
        public boolean onClick(int xTile, int yTile) {
            from.setPosition(yTile, xTile);
            to.setPosition(yTile, xTile);
            panel.getTile(xTile,yTile).setSelect(true);
            return true;
        }

        @Override
        public boolean onDrag(int xFrom, int yFrom, int xTo, int yTo) {
            from.setPosition(yFrom, xFrom);
            to.setPosition(yTo, xTo);

            panel.getTile(xFrom,yFrom).setSelect(false);

            if (model.drag(from, to)) {
                if (model.isOver()) {
                    Toast.makeText(getApplicationContext(), R.string.LevelCompleteString, Toast.LENGTH_LONG).show();
                    panel.removeHeartbeatListener();
                    levelCompleted = true;
                    saveLastLevel();
                    nextLevelBtn.setEnabled(true);
                }
            }

            panel.invalidate();
            return true;
        }

        @Override
        public void onDragEnd(int x, int y) {
            panel.getTile(x,y).setSelect(false);
            if (from.equals(to) && model.unlink(from));
                panel.invalidate();
        }

        @Override
        public void onDragCancel() {}
    };


    /* --------------------------- PANEL LISTENER ---------------------------   */


    private OnBeatListener startBeatListener() {
        return new OnBeatListener() {
            long minutes = 0, seconds = 0;

            @Override
            public void onBeat(long beat, long time) {
                elapsedTime = elapsedTime + 1;
                minutes = elapsedTime / 60;
                seconds = elapsedTime % 60;
                timerTxtView.setText(String.format("%02d:%02d", minutes, seconds));
                timerTxtView.invalidate();
            }
        };
    }

    /* --------------------------- NEW LEVEL BUTTON ---------------------------   */
    View.OnClickListener nextLevelBtnList = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
                elapsedTime = 0;
                levelCompleted = false;
                loadLevel(++level);
                updateLevelTxtView();
                loadViewsById();
                panel.setHeartbeatListener(1000, startBeatListener());
            } catch (Loader.LevelFormatException e) {
                nextLevelBtn.setEnabled(false);
                Toast.makeText(getApplicationContext(), R.string.NoMoreLevels, Toast.LENGTH_SHORT).show();
                resetLevels();
            }
        }
    };


    /* --------------------------- LINK VIEW RESOURCES ---------------------------   */
    String levelText = new String();

    private void loadViewsById() {
        timerTxtView = (TextView) findViewById(R.id.TimerTextViewId);
        timerTxtView.setText(R.string.TimerDefault);

        /* ---- Next Level Button ---- */
        levelTxtView = (TextView) findViewById(R.id.LevelTxtViewId);
        updateLevelTxtView();


        /* ---- Next Level Button ---- */
        nextLevelBtn = (Button) findViewById(R.id.NextLevelBtnId);
        nextLevelBtn.setEnabled(false);
        nextLevelBtn.setOnClickListener(nextLevelBtnList);

        /* ---- Load Panel ---- */
        panel = (Panel) findViewById(R.id.gridPanelId);
        panel.setSize(model.getLines(), model.getColumns());
        panel.setListener(panelListener);

        for (int l = 0; l < model.getLines(); l++)
            for (int c = 0; c < model.getColumns(); c++)
                panel.setTile(c, l, CellView.newInstance(model.getCell(l, c)));

        panel.invalidate();
    }


    /* --------------------------- LEVEL LOADER ------------------------------------   */
    private void loadLevel(int n) throws Loader.LevelFormatException {
        AssetManager assets = getAssets();
        Scanner in = null;
        try {
            in = new Scanner(
                    assets.open("levels.txt")
            );
            model = new Loader(in).load(n);
        }
        catch (IOException e) {
            Log.e("Layout", e.getMessage());
        }
        finally {
            if (in!=null) in.close();
        }
    }

    private void updateLevelTxtView() {
        if (level > 0)
            levelText = getResources().getString(R.string.LevelTxtViewString) + " " + level;
        else
            levelText = "No more levels!";
        levelTxtView.setText(levelText);
    }

    private void saveLastLevel() {
        try {
            FileOutputStream fileOutputStream = openFileOutput(LAST_LEVEL_FILENAME, MODE_PRIVATE);
            LevelFileAccess.saveLastCompletedLevel(fileOutputStream, level);
        } catch (IOException e) {
            Log.e("ERROR", e.getMessage());
        }
    }

    private void loadLastLevel() {
        try {
            FileInputStream fileInputStream = openFileInput(LAST_LEVEL_FILENAME);
            level = LevelFileAccess.loadLastCompletedLevel(fileInputStream);
        } catch (IOException e) {
            level = 0;
            Log.e("ERROR", e.getMessage());
        }

    }

    private void resetLevels() {
        level = 0;
        saveLastLevel();
    }
}
