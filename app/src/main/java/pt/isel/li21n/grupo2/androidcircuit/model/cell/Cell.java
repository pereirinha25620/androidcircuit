package pt.isel.li21n.grupo2.androidcircuit.model.cell;

import java.util.LinkedList;

import pt.isel.li21n.grupo2.androidcircuit.model.Dir;
import pt.isel.li21n.grupo2.androidcircuit.model.Position;

public abstract class Cell {

    private static final String FIELD_TOKEN = ";";

    protected char type;
    protected Position pos;
    protected LinkedList<Dir> connections;

    public static Cell newInstance(char cellType, Position pos) {
        if (cellType == '.')
            return new Free(cellType, pos);
        if (cellType == '-')
            return new HorLine(cellType, pos);
        if (cellType == '|')
            return new VertLine(cellType, pos);
        if (cellType >= 'A' && cellType <= 'F')
            return new Terminal(cellType, pos);
        if (cellType == '*')
            return new Block(cellType, pos);
        return null;
    }


    public Cell(char type, Position pos) {
        this.type = type;
        this.pos = pos;
        connections = new LinkedList<>();
    }


    // Return cell "type" as an integer to be used as index in COLOR array
    public int getColor() {
        return type - 'A';
    }


    // Return cell coordinates in model
    public int getLine() {
        return (int) pos.getLine();
    }

    public int getCol() {
        return (int) pos.getCol();
    }


    // ABSTRACT CELL METHODS
    public abstract boolean isConnected();

    public abstract boolean connectCell(Dir dir);

    public abstract boolean unlinkCell(Dir dir);

    public abstract Dir getNextDirection(Dir dir);

    public abstract void resetCell();

    public abstract boolean drag(Dir dir);


    public Dir getConnection() {
        return connections.getLast();
    }

    public LinkedList<Dir> getConnections() {
        return connections;
    }

    public boolean isInPath() {
        return !connections.isEmpty();
    }

    public char getType() {
        return type;
    }

    public void setType(char type) {
        this.type = type;
    }

    public void addConnection(Dir dir) {
        connections.add(dir);
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder(type + FIELD_TOKEN + pos);

        if (!connections.isEmpty())
            for (Dir dir : connections)
                out.append(FIELD_TOKEN + dir.toString());

        return out.toString();
    }

}
