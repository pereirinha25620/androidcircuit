package pt.isel.li21n.grupo2.androidcircuit.model;

public class Position {

    private static final String COORD_TOKEN = ":";

    private int line;
    private int col;

    public Position() {
    }

    public Position(int newLine, int newCol) {
        line = newLine;
        col = newCol;
    }

    public Position(String positionStr) {
        String[] coords = positionStr.split(String.valueOf(COORD_TOKEN));
        line = Integer.parseInt(coords[0]);
        col = Integer.parseInt(coords[1]);
    }

    public float getLine() {
        return line;
    }

    public float getCol() {
        return col;
    }

    public void setPosition(int line, int col) {
        this.line = line;
        this.col = col;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj instanceof Position) {
            Position other = (Position) obj;
            if (other.getLine() == line && other.getCol() == col) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return line + COORD_TOKEN + col;
    }
}

