package pt.isel.li21n.grupo2.androidcircuit.model.cell;

import pt.isel.li21n.grupo2.androidcircuit.model.Dir;
import pt.isel.li21n.grupo2.androidcircuit.model.Position;

public abstract class Node extends Cell {

    protected char startType;

    public Node(char type, Position pos) {
        super(type, pos);
        startType = type;
    }


    @Override
    public boolean isConnected() {
        return connections.size() == 2;
    }

    @Override
    public boolean connectCell(Dir dir) {
        if (connections.isEmpty() || connections.size() < 2) {
            connections.add(dir);
            return true;
        }
        return false;
    }

    @Override
    public boolean unlinkCell(Dir dir) {
        if (!connections.isEmpty()) {
            connections.remove(dir);

            if (connections.isEmpty()) {
                type = startType;
            }
            return true;
        }
        return false;
    }

    @Override
    public Dir getNextDirection (Dir dir) {
        if (connections.isEmpty()) {
            return dir;
        }

        if (connections.contains(dir.invertDir())) {
            if (connections.size() == 1) {
                return dir;
            } else {
                if (dir.invertDir() == connections.getLast())
                    return connections.getFirst();
                else
                    return connections.getLast();
            }
        }

        return dir;
    }

    @Override
    public void resetCell() {
        connections.clear();
        type = startType;
    }

    public void updateType(char newType) {
        if (type == startType)
            type = newType;
        else
            type = startType;
    }

    @Override
    public String toString() {
        return super.toString() + ";" + startType;

    }
}
