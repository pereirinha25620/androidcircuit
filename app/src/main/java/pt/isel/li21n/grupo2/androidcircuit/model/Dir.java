package pt.isel.li21n.grupo2.androidcircuit.model;

public enum Dir {

    //      dLin   dCol
    LEFT    ( 0,   -1),
    RIGHT   ( 0,    1),
    DOWN    ( 1,    0),
    UP      (-1,    0);

    private int dLin;
    private int dCol;

    Dir(int dLin, int dCol) {
        this.dLin = dLin;
        this.dCol = dCol;
    }

    public int getDeltaLin() {
        return dLin;
    }

    public int getDeltaCol() {
        return dCol;
    }
    
    public Dir invertDir() {
        if (this == Dir.DOWN)
            return Dir.UP;
        if (this == Dir.UP)
            return Dir.DOWN;
        if (this == Dir.LEFT)
            return Dir.RIGHT;
        if (this == Dir.RIGHT)
            return Dir.LEFT;
        return null;
    }

}

