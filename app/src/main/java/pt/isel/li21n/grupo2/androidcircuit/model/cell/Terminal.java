package pt.isel.li21n.grupo2.androidcircuit.model.cell;


import pt.isel.li21n.grupo2.androidcircuit.model.Dir;
import pt.isel.li21n.grupo2.androidcircuit.model.Position;

public class Terminal extends Cell {

    public Terminal(char type, Position pos) {
        super(type, pos);
    }


    @Override
    public boolean isConnected() {
        return connections.size() == 1;
    }

    @Override
    public boolean connectCell(Dir dir) {
        if (connections.isEmpty()) {
            connections.add(dir);
        }
        return isConnected();
    }

    @Override
    public boolean unlinkCell(Dir dir) {
        if (isConnected()) {
            connections.remove(dir);
        }
        return isConnected();
    }

    @Override
    public Dir getNextDirection(Dir dir) {
        if (dir.equals(this.getConnection().invertDir())) {
            return this.getConnection().invertDir();
        }
        return dir;
    }

    @Override
    public boolean drag(Dir dir) {
        return true;
    }

    @Override
    public void resetCell() {
        connections.clear();
    }

    @Override
    public Dir getConnection() {
        if (!connections.isEmpty())
            return connections.getLast();
        return null;
    }
}
