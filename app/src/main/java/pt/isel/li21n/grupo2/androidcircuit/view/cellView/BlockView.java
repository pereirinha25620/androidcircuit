package pt.isel.li21n.grupo2.androidcircuit.view.cellView;

import android.graphics.Canvas;

import pt.isel.li21n.grupo2.androidcircuit.model.cell.Cell;

public class BlockView extends CellView {

    private static final float BLOCK_OFFSET = 1f/5;

    public BlockView(Cell cell) {
        super(cell);
    }

    @Override
    protected void drawDefaultCell(Canvas canvas, int side) {
        canvas.drawRect(
                side*BLOCK_OFFSET, side*BLOCK_OFFSET,
                side-side*BLOCK_OFFSET, side-side*BLOCK_OFFSET,
                defaultPaint
        );
    }

    @Override
    protected void drawConnections(Canvas canvas, int side) {}
}
