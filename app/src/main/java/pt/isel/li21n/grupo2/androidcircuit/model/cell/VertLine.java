package pt.isel.li21n.grupo2.androidcircuit.model.cell;

import pt.isel.li21n.grupo2.androidcircuit.model.Dir;
import pt.isel.li21n.grupo2.androidcircuit.model.Position;

public class VertLine extends Node {

    public VertLine(char type, Position pos) {
        super(type, pos);
    }

    @Override
    public boolean drag(Dir dir) {
        return dir == Dir.UP || dir == Dir.DOWN;
    }
}
