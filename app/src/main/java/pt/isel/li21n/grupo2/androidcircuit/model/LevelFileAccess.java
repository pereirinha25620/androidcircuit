package pt.isel.li21n.grupo2.androidcircuit.model;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Scanner;

public class LevelFileAccess {

    public static void saveLastCompletedLevel(OutputStream fileOutputStream, int level) throws IOException {
        BufferedWriter bufferedWrite = new BufferedWriter(
                new OutputStreamWriter(fileOutputStream)
        );

        // Write last completed level
        bufferedWrite.write(String.valueOf(level));
        bufferedWrite.newLine();

        bufferedWrite.close();
    }

    // Load last sketch stored
    public static int loadLastCompletedLevel(InputStream inputStream) throws IOException {
        Scanner in = new Scanner(inputStream);

        return Integer.parseInt(in.nextLine());
    }
}
