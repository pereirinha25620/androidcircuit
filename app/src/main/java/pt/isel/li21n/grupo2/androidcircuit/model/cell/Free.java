package pt.isel.li21n.grupo2.androidcircuit.model.cell;

import pt.isel.li21n.grupo2.androidcircuit.model.Dir;
import pt.isel.li21n.grupo2.androidcircuit.model.Position;

public class Free extends Node {

    public Free(char type, Position pos) {
        super(type, pos);
    }

    @Override
    public boolean drag(Dir dir) {
        return true;
    }

}
