package pt.isel.li21n.grupo2.androidcircuit.view.cellView;

import android.graphics.Canvas;

import pt.isel.li21n.grupo2.androidcircuit.model.Dir;
import pt.isel.li21n.grupo2.androidcircuit.model.cell.Cell;

public class TerminalView extends CellView {

    public TerminalView(Cell cell) {
        super(cell);
        this.setColor(CellView.getColor(cell));
    }

    @Override
    protected void drawDefaultCell(Canvas canvas, int side) {
        canvas.drawCircle(side/2, side/2, side * TERMINAL_RADIUS, cellPaint);

        canvas.drawCircle(side/2, side/2, side * CENTER_RADIUS, defaultPaint);
    }

    @Override
    protected void drawConnections(Canvas canvas, int side) {
        for (Dir dir : cell.getConnections())
            canvas.drawLine(
                    side/2, side/2,
                    side*CellView.DIR_TO_COORDS[dir.ordinal()*2], side*CellView.DIR_TO_COORDS[dir.ordinal()*2+1],
                    cellPaint
            );

        drawDefaultCell(canvas, side);
    }
}
